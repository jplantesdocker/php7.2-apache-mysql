# Entorno básico para desarrollo en PHP 7.2 con MariaDB

La idea es armar un pequeño entorno de desarrollo en PHP.
La imagen esta basada en la imagen php7.2-apache de Docker, a la cual se le agregan las extensiones de mysqli y pdo_mysql.

## Construir el entorno

* Clonar proyecto

```
$ git clone https://jplantes@bitbucket.org/jplantesdocker/php7.2-apache-mysql.git
```
* Crear directorios "php" y "datadb"

```
$ cd php7.2-apache-mysql
$ mkdir php
$ mkdir datadb
```

* Quitamos del archivo .gitignore el directorio "php"

------------------------------------------------------------------------------------

Quenes trabajen con Linux pueden ejecutar el archivo inicializado.sh
Este archivo realiza las dos acciones antes descripts

```
bash inicializador.sh
```
------------------------------------------------------------------------------------

Todo nuestra aplicación deberá ir en el directorio "php"


* Levantar entorno

```
$ docker-compose up -d
```

* Bajar entorno

```
$ docker-compose down
```



## Detalle de Dockerfile

```
#Imagen base
FROM php:7.2-apache

#Librerias añadidas
RUN docker-php-ext-install mysqli pdo_mysql

EXPOSE 80
```

## Detalles de docker-compose.yml

```
version: '3.3'

services:
  # Apache con PHP 7.2
  php:
    container_name: php7.2-dev
    #Compila el Dockerfile que se encuentra en este directorio con las extensiones para la conexión a la BBDD
    build: .
    volumes:
      - ./php:/var/www/html
    ports:
      - 8099:80
    depends_on:
      - mariadb
    links:
      - mariadb
  # Motor de BBDD MariaDB
  mariadb:
    container_name: marindb-dev
    image: mariadb
    volumes:
      - ./datadb:/var/lib/mysql
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: root
  # Administrador liviano para BBDD
  adminer:
    container_name: adminer-dev
    image: adminer
    ports:
      - 8080:8080
```