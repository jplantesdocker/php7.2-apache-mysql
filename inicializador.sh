#!/bin/bash

#Compruebo y creo el directorio "php"
if [ ! -d ./php ]; then
  echo -e "> Se creo el directorio \"php\" \n"
  mkdir php
fi

#Compruebo y creo el directorio "datadb"
if [ ! -d ./datadb ]; then
  echo -e "> Se creo el directorio \"php\" \n"
  mkdir datadb
fi

#Elimino del .gitignore el directorio "php"
sed -i 's/php\///g' .gitignore
echo "> Se elimino del archivo .gitignore el directorio php \n"
